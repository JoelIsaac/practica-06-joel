from src.nodos import Nodo


# Definicion de la clase 'ListaLigada':
class ListaLigada:
    def __init__(self):
        self.cabeza = None

    # Agrega un elemento al final de la lista
    def agregar_elemento(self, valor):
        # Aqui inicia tu codigo :D
        nuevo_nodo = Nodo(valor)
        #Si la cabeza esta vacia
        if(self.cabeza == None):
            self.cabeza = nuevo_nodo
            return
        #Cuando no esta vacia recorremos los nodos y agregamos al final
        puntero = self.cabeza
        while (puntero.siguiente):
            puntero = puntero.siguiente
        puntero.siguiente = nuevo_nodo


    def buscar_elemento(self, valor):
        nodo_a_comparar = Nodo(valor)
        if(self.cabeza == None):
            return False
        #Mientras los valores de los nodos sena distintos recorremos el siguiente nodo
        puntero = self.cabeza
        while(puntero.siguiente):
            if puntero.valor == nodo_a_comparar.valor:
                return True
            puntero = puntero.siguiente
        return False

    def elimina_cabeza(self):
        # Aqui inicia tu codigo :D
        if(self.cabeza == None):
            return self.cabeza
        self.cabeza = self.cabeza.siguiente
        return

    def elimina_rabo(self):
        # Aqui inicia tu codigo
        #Lista vacia
        if(self.cabeza == None):
            return self.cabeza
        #Un solo elemento que apunta de por si a none lo eliminamos
        if(self.cabeza.siguiente == None):
            self.cabeza = None
            return
        #Encontramos el nodo al que su siguiente haga referencia a None y lo eliminamos
        puntero = self.cabeza
        while (puntero.siguiente.siguiente):
            puntero = puntero.siguiente
        puntero.siguiente = None
        return

    def tamagno(self):
        # Aqui inicia tu codigo
        contador = 0

        if(self.cabeza == None):
            return contador 

        puntero = self.cabeza
        while(puntero.siguiente):
            contador +=1
            puntero = puntero.siguiente
        contador +=1
        return contador

    def copia(self):
        nueva_lista = ListaLigada()
        actual = self.cabeza
        while actual:
            nueva_lista.agregar_elemento(actual.valor)
            actual = actual.siguiente
        return nueva_lista

    def __str__(self):
        valores = []
        actual = self.cabeza
        while actual:
            valores.append(str(actual.valor))
            actual = actual.siguiente
        return " -> ".join(valores)
